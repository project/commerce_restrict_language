Configuring Commerce Restrict Language

Languages are not restricted by default. To disable commerce in a language, edit
it from the Languages administration page at /admin/config/regional/language
(Administration » Configuration » Regional and language » Languages). There are
checkboxes on the individual language's form that can be unticked to disable
parts of commerce in the language, such as the cart and checkout or displaying
prices.
